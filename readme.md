# Users Service

Users Service é uma api desenvolvida com objetivos acadêmicos, proposta pela Kenzie Academy.

## Como instalar?

Faça o clone do repositório:

<https://gitlab.com/alesilvasp/entrega2-users-service>

Entre na pasta e rode o comando no terminal para instalar as dependências:

`yarn install`

Depois de instaladas as dependências, rode o comando no terminal: 

`yarn dev`

ou

`npm run dev`

O sistema estará rodando em https://localhost:3000

## Utilização 

Para utilizar este sistema, é necessário utilizar um API Client, como o Insomnia ou Postman

### Rotas

**POST /signup**

Feita para cadastrar um novo usuário

RESPONSE STATUS -> HTTP 201(CREATED)

Body:

```json
{
	"username": "alexander",
	"age": 35,
	"email": "email@email.com",
	"password": "12345678"
}
```

Response:

```json 
{
	"id": "8f08e309-ea59-4b8e-8ed0-9a0c69c541a0",
	"createdAt": "2022-01-13T14:43:35.813Z",
	"email": "email@email.com",
	"age": 35,
	"username": "alexander"
}
```
**POST /login**

Logar na api informando *username* e *password*

RESPONSE STATUS -> HTTP 200(OK)

Body:

```json
{
	"username": "alexander",
	"password": "12345678"
}
```

Response: 
```json
{
	"accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFsZXhhbmRlciIsImlhdCI6MTY0MjA4NTIzOCwiZXhwIjoxNjQyMDg4ODM4fQ.t8RmmoX4PDN817xf2p0dgwch0JoV0COpiqAigz1p7fw"
}
```
**GET /users**

Retorno de todos os usuários cadastrados

RESPONSE STATUS -> HTTP 200(OK)

Autorization Header:

**Necessário passar o token para autenticação**

```json
[
	{
		"id": "19184f75-244c-45a1-acfd-eec9d80c9029",
		"createdAt": "2022-01-13T14:57:28.726Z",
		"email": "email@email.com",
		"age": 35,
		"username": "alexander"
	},
	{
		"id": "c03a6e67-cc6e-4095-a057-7d00d01b78e6",
		"createdAt": "2022-01-13T14:57:49.559Z",
		"email": "Mail@Mail.com",
		"age": 40,
		"username": "Silva"
	}
]
```
**PUT /users/:id/password**

Rota para alteração de senha, passando o id por parâmetro

RESPONSE STATUS -> HTTP 204(NO CONTENT)

Autorization Header:

**Necessário passar o token para autenticação e autorização**

Body:

```json 
{
	"password": "87654321"
}
```
Response:

*No body returned for response*

## Tecnologias utilizadas

- Express.js
- Nodemon
