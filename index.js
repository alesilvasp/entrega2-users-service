import express from "express";
import { v4 as uuidv4 } from "uuid";
import * as yup from "yup";
import jwt from "jsonwebtoken";
import * as bcrypt from "bcryptjs";

const app = express();

const config = {
  secret: "my_random_secret_key",
  expiresIn: "1h",
};

app.use(express.json());

const users = [];

const userSchema = yup.object().shape({
  username: yup.string().required(),
  age: yup.number().required().positive().integer(),
  email: yup.string().email().required(),
  password: yup.string().required().min(8),
  createdAt: yup
    .date()
    .default(function () {
      return new Date();
    })
    .transform(function () {
      return new Date();
    }),
  id: yup
    .string()
    .uuid()
    .default(function () {
      return uuidv4();
    })
    .transform(function () {
      return uuidv4();
    }),
});

const validateUser = (schema) => async (req, res, next) => {
  const data = req.body;
  try {
    const newUser = await userSchema.validate(data, {
      abortEarly: false,
      stripUnknown: true,
    });
    req.newUser = newUser;
    next();
  } catch (e) {
    console.error(e);
    res.status(422).json({ error: e.errors.join(", ") });
  }
};

const authenticateUser = (req, res, next) => {
  let token = req.headers.authorization.split(" ")[1];

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).json({ message: "Invalid Token." });
    }
    let user = users.find((user) => user.username === decoded.username);

    req.user = user;
  });
  next();
};

const authorizeUser = (req, res, next) => {
  const { id } = req.params;
  const { user } = req;

  if (id !== user.id) {
    return res.status(401).json({ message: "Not Allowed" });
  }
  next();
};

app.post("/signup", validateUser(userSchema), async (req, res) => {
  const { newUser } = req;

  try {
    const hashedPassword = await bcrypt.hash(newUser.password, 10);
    newUser.password = hashedPassword;

    users.push(newUser);

    const { password: user_password, ...userWithotPassword } = newUser;

    return res.status(201).json(userWithotPassword);
  } catch (e) {
    res.json({ message: "Error while creating an user" });
  }
});

app.post("/login", async (req, res) => {
  let { username, password } = req.body;
  const user = await users.find((user) => user.username === username);

  try {
    const match = await bcrypt.compare(password, user.password);
    let token = jwt.sign(
      { username: username, uuid: user.uuid },
      config.secret,
      { expiresIn: config.expiresIn }
    );
    if (match) {
      res.json({ accessToken: token });
    } else {
      res.status(401).json({ message: "Invalid Credentials" });
    }
  } catch (e) {
    console.log(e);
  }
});

app.get("/users", authenticateUser, (req, res) => {
  return res.json(users);
});

app.put(
  "/users/:id/password",
  authenticateUser,
  authorizeUser,
  async (req, res) => {
    const { password } = req.body;
    const { user } = req;
    const hashedPassword = await bcrypt.hash(password, 10);
    user.password = hashedPassword;
    res.status(204).json(user);
  }
);

app.listen(3000, () => {});
